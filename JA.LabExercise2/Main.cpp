#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank {
	ACE = 14,
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING
};

enum Suit {
	CLUBS = 1,
	DIAMONDS,
	HEARTS,
	SPADES
};

struct Card {
	Rank rank;
	Suit suit;
};

void PrintCard(Card card);
Card HighCard(Card card1, Card card2);

int main() {



	_getch();
	return 0;
}

void PrintCard(Card card)
{
    string rank="";
    string suit="";
	
    switch (card.rank)
    {
    case 14:
        rank="ACE";
        break;
    case 2:
        rank="TWO";
        break;
    case 3:
         rank="THREE";
        break;
    case 4:
         rank="FOUR";
        break;
    case 5:
         rank="FIVE";
        break;
    case 6:
         rank="SIX";
        break;
    case 7:
        rank="SEVEN";
        break;
    case 8:
         rank="EIGHT";
        break; 
    case 9:
         rank="NINE";
        break;
    case 10:
        rank="TEN";
        break;
     case 11:
        rank="JACK";
        break;    
    case 12:
       rank= "QUEEN";
        break;
    case 13:
         rank="KING";
        break;    
    }
    
      switch (card.suit)
    {
    case 1:
        suit="CLUBS";
        break;
    case 2:
        suit="DIAMONDS";
        break;
    case 3:
        suit="HEARTS";
        break;
    case 4:
        suit="SPADES";
        break;
    
       
    } 
    
	cout<<"The "<< rank <<" of "<< suit <<endl;
}
Card HighCard(Card card1, Card card2){
    if(card1.rank > card2.rank)
      return card1;
    else{
        return card2;
}
